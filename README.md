## Flask Portfolio App

To install and run this app you first need to install the following things:

+ MongoDB
+ Flask framework
+ Flask-PyMongo

To install Mongo go to https://fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-4.0.4-signed.msi


To install flask:

     sudo apt install virtualenv
       virtualenv flaskapp
       cd flaskapp
       source bin/activate
       pip install Flask

To install flask-PyMongo:

		pip install Flask-PyMongo

Run the app with:

		python routes.py

Access the app at the IP:

		127.0.0.1:5000

