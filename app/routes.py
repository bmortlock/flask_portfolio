from flask import Flask, render_template

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/portfolio"
mongo = PyMongo(app)

@app.route('/')

def home():
 return render_template('home.html')

@app.route('/work')
def work():
    works_data = mongo.db.works.find()
    return render_template("work.html",
                            works=works_data)



if __name__ == '__main__':
  app.run(debug=True)